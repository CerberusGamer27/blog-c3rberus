import rss, {pagesGlobToRssItems} from "@astrojs/rss";

export async function GET(){
    return rss({
        title: "c3rberus | Blog",
        description: "c3rberus.dev blog to share tutorials an tips.",
        site: "https://my-blog-site.netlify.app",
        items: await pagesGlobToRssItems(import.meta.glob("./**/*md")),
        customData: `<language>en-us</language>`
    })
}