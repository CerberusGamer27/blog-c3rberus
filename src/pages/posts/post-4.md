---
layout: ../../layouts/MarkdownPostLayout.astro
title: 'My Fourth Blog Post'
pubDate: 2023-08-13
description: 'This is the first post of my new Astro blog.'
author: 'c3rberus.dev'
image:
    url: 'https://raw.githubusercontent.com/withastro/astro/main/.github/assets/banner.png' 
    alt: 'The Astro logo with the word One.'
tags: ["astro", "blogging", "learning in public"]
---
#### Using the Astro API

>This post should show up with my other blog posts, because `Astro.glob()` is returning a list of all my posts in order to create my list.