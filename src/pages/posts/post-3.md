---
layout: ../../layouts/MarkdownPostLayout.astro
title: My Third Blog Post
author: Astro Learner
description: "I had some challenges, but asking in the community really helped!"
image: 
    url: "https://raw.githubusercontent.com/withastro/astro/main/.github/assets/banner.png"
    alt: "The word community with a heart."
pubDate: 2023-02-09
tags: ["astro", "learning in public", "setbacks", "community"]
---
It wasn't always smooth sailing, but I'm enjoying building with Astro. And, the [Discord community](https://astro.build/chat) is really friendly and helpful!